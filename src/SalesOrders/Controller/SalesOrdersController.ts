import { AxiosResponse } from 'axios';

import { Iinfra} from '../../../infra/services';//PACKAGE INFRA

import type { SalesOrdersEntity } from '../Domain/SalesOrders/SalesOrdersEntity';
import {
  InterfaceUCSalesOrders
} from '../Domain/UsesCases/SalesOrders';



export interface InterfaceSalesOrdersService {
  infra: Iinfra;
  UCSalesOrders: InterfaceUCSalesOrders;
  listSalesOrdersParis: (rut: string) => Promise<SalesOrdersEntity[]>;
  listSalesOrdersTodos: () => Promise<AxiosResponse>;
}


export class SalesOrdersService implements InterfaceSalesOrdersService {

  public UCSalesOrders: InterfaceUCSalesOrders;
  public infra:Iinfra;

  constructor({infra, UCSalesOrders}: {infra:Iinfra, UCSalesOrders: InterfaceUCSalesOrders}) {
    this.UCSalesOrders = UCSalesOrders;
    this.infra = infra;
  }

  async listSalesOrdersTodos(): Promise<any> {
    try {
      let result = await this.infra.fetchers.salesOrders.todos();
      return this.UCSalesOrders.listTodos(result);
    } catch (e) {
      console.log('e?', e);
      throw new Error('No se pudo obtener el listado');
    }
  }
  async listSalesOrdersParis(rut: string): Promise<any> {
    try {
      let result = await this.infra.fetchers.salesOrders.paris(rut);
      return this.UCSalesOrders.list(result.data.data.ordersPagination.data);
    } catch (e) {
      console.log('e?', e);
      throw new Error('No se pudo obtener el listado');
    }
  }
}