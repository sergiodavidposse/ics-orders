import type { SalesOrdersEntity } from '../SalesOrders/SalesOrdersEntity';
import {
  InterfaceSalesOrdersModel,
  SalesOrdersModel,
} from '../SalesOrders/SalesOrdersModel';

export interface InterfaceUCSalesOrders {
  list: (salesOrders: SalesOrdersEntity[]) => SalesOrdersEntity[];
  listTodos: (array: string[]) => string[];
}

export class UCSalesOrders implements InterfaceUCSalesOrders {
  protected salesOrdersModel: InterfaceSalesOrdersModel;
  constructor({SalesOrdersModel}:{SalesOrdersModel:InterfaceSalesOrdersModel}) {
    this.salesOrdersModel = SalesOrdersModel;
  }
  list = (listSales: SalesOrdersEntity[]): SalesOrdersEntity[] => {
    return this.salesOrdersModel.list(listSales);
  };
  listTodos = (list:string[]): string[] => {
    return this.salesOrdersModel.listTodos(list);
  };
}
