import { SalesOrdersModel } from './SalesOrdersModel';
import { SalesOrdersEntity } from './SalesOrdersEntity';

export { SalesOrdersEntity, SalesOrdersModel };
