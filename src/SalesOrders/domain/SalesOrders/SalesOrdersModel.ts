import type { SalesOrdersEntity } from './SalesOrdersEntity';

export interface InterfaceSalesOrdersModel {
  list: (salesOrders: SalesOrdersEntity[]) => SalesOrdersEntity[];
  listTodos: (array:string[]) => string[]
}

export class SalesOrdersModel implements InterfaceSalesOrdersModel {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}
  list = (salesOrders: SalesOrdersEntity[]) => salesOrders;
  listTodos = (array:string[]) => array;
}
