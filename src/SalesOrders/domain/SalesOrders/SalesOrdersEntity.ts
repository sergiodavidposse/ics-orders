type Payments = {
  method_name: string;
};

type Billing_info = {
  folio: number;
  external_order_number: number;
  transaccion: string;
  terminal: number;
  dte_url: string;
};

type Customer = {
  first_name: string;
  last_name: string;
  identity_document_number: string;
  email: string;
  phone_number: string;
};

type Order_items_attributes = {
  name: string;
  value: string;
};

type Order_items = {
  sku: string;
  title: string;
  gross_price: number;
  external_order_line_number: string;
  delivery_date: string;
  price_after_discounts: number;
  seller_id?: string;
  quantity: number;
  options: any[];
  attributes: Order_items_attributes[];
};

type Shipments = {
  external_order_number: string;
};

export type SalesOrdersEntity = {
  order_number: string;
  invoice_type: string;
  origin_completed_at: string;
  origin_order_number: string;
  payments: Payments[];
  total_gross_price: number;
  total_net_price: number;
  billing_info: Billing_info[];
  customer: Customer;
  order_items: Order_items[];
  shipments: Shipments[];
};
