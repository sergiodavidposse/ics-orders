import {
  createContainer,
  asClass,
  InjectionMode,
  asFunction,
} from 'awilix';

import { SalesOrdersModel } from './Domain/SalesOrders';
import { SalesOrdersService } from './Controller';
import { UCSalesOrders } from './Domain/UsesCases';
import {infraAwilix} from '../../infra';//PACKAGE INFRA

const container = createContainer({
  injectionMode: InjectionMode.PROXY,
});

container.register({
  SalesOrdersService: asClass(SalesOrdersService).singleton(),
	UCSalesOrders: asClass(UCSalesOrders).singleton(),
  SalesOrdersModel: asClass(SalesOrdersModel).singleton(),
  infra: asFunction(()=>infraAwilix),
});

export default container;