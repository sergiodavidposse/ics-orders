import { SalesOrdersService } from '../index';
import { AdapterFetch } from '../../infra/fetch/adapterFetch';

jest.setTimeout(15000);

let response: any;
let salesOrdersService = new SalesOrdersService(AdapterFetch);

beforeEach(async () => {
  response = await salesOrdersService.listSalesOrders('150356873');
});

describe('Testing listSalesOrders', () => {
  it('Response debe estar definida', async () => {
    console.log(response);
    expect(response).toBeDefined();
  });
});
