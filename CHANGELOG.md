# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.4](https://gitlab.com/sergiodavidposse/ics-orders/compare/v1.0.3...v1.0.4) (2022-01-12)

### [1.0.3](https://gitlab.com/sergiodavidposse/ics-orders/compare/v1.0.2...v1.0.3) (2022-01-05)

### [1.0.2](https://gitlab.com/sergiodavidposse/ics-orders/compare/v1.0.1...v1.0.2) (2022-01-04)

### [1.0.1](https://gitlab.com/sergiodavidposse/ics-orders/compare/v1.0.5...v1.0.1) (2022-01-04)

### 1.0.5 (2022-01-04)
