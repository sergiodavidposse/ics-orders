import container2 from './container';
import {infra} from './services';
import { Iinfra } from './services';

const infraAwilix = container2.resolve('infraestructura');

export{Iinfra, infra, infraAwilix};