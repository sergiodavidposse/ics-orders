import {
  createContainer,
  asClass,
  InjectionMode,
} from 'awilix';

import fetchers from './services/ports/fetchers';
import salesOrdersRepository from './services/fetchers/adapters/salesOrdersRepository'
import {infra} from './services';

const container2 = createContainer({
  injectionMode: InjectionMode.PROXY,
});

container2.register({
  fetchers: asClass(fetchers).singleton(),
	infraestructura: asClass(infra).singleton(),
	salesOrdersRepository: asClass(salesOrdersRepository).singleton(),
});

export default container2;
