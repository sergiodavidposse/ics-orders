import { ISalesOrdersRepository } from '../fetchers/adapters/SalesOrdersRepository';

export interface Ifetchers {
  salesOrders: ISalesOrdersRepository;
}

export default class fetchers implements Ifetchers {
  public salesOrders: ISalesOrdersRepository;
  constructor({salesOrdersRepository}:{salesOrdersRepository: ISalesOrdersRepository}) {
    this.salesOrders = salesOrdersRepository;
  }
}
