import { AxiosResponse } from 'axios';
import baseFetchersRepository from '../baseRepository';
import { SALES_ORDERS_SERVICE_URL, SALES_ORDERS_SERVICE_KEY } from '../../../../config';

export interface ISalesOrdersRepository {
  todos: () => Promise<string[]>;
  numero1: () => Promise<string[]>;
  paris: (rut: string) => Promise<AxiosResponse>;
}

export default class salesOrdersRepository
  extends baseFetchersRepository
  implements ISalesOrdersRepository
{
  protected configParams: any;
  protected axiosInstance: any;

  constructor() {
    super();
    this.axiosInstance = this.axios.create({
      baseURL: SALES_ORDERS_SERVICE_URL,
      params: {
        'x-api-key': SALES_ORDERS_SERVICE_KEY,
      },
    });
  }
  todos = async () => {
    return await this.obtener('https://jsonplaceholder.typicode.com/todos');
  };
  numero1 = async () => {
    return await this.obtener('/todos/1');
  };

  paris = async (rut: string) => {
    
    const filterParisGraphql = {
      paymentStatus: 'PAID',
      company: { name: 'PARIS' },
      pagination: { from: 1, size: 5 },
      identity_document_number: rut,
      range: {
        rangeType: 'creationDate',
        from: '2021-09-09',
        to: '2021-12-09',
      },
      sku_summary: true,
    };

    const body = {
      query:`query getOrdersPagination($filter: OrderInput!) {

        ordersPagination(filters: $filter) {
        
        data {
        
        order_number
        
        total_gross_price
        
        company_name
        
        invoice_type
        
        origin_completed_at
        
        origin_order_number
        
        payments {
        
        method_name
        
        }
        
        total_net_price
        
        customer {
        
        first_name
        
        last_name
        
        identity_document_number
        
        email
        
        phone_number
        
        }
        
        order_items {
        
        sku
        
        title
        
        gross_price
        
        external_order_line_number
        
        delivery_date
        
        price_after_discounts
        
        seller_id
        
        quantity
        
        }
        
        }
        
        pagination {
        
        page
        
        size
        
        totalCount
        
        totalPage
        
        }
        
        }
        
        }`,
        variables: {

          filter: {
          
          paymentStatus: 'PAID',
          
          pagination: { from: 1, size: 5 },
          
          identity_document_number: rut,
          
          },
          
          },
    };

    let res = await this.axiosInstance.post('', body, this.configParams);
    if (res.data.errors) {
        console.log(res.data.errors);
        return res.data.errors;
    }
    return res;
  };
}
