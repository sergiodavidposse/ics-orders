import { abstractBaseRepository } from '../../abstracts/abstractBaseRepository';
import axios from 'axios';

export default class baseFetchersRepository extends abstractBaseRepository {
  protected axios:any;

  constructor(){
    super()
    this.axios = axios;
  }
  
  protected async obtener(url: string) {
    return await this.axios.get(url);
  }
  protected async enviar(url: string, body:any, configParams:any) {
    return await this.axios.post('', body, configParams);
  }
  protected actualizar(url: string) {
    return this.axios.put(url);
  }
  protected eliminar(url: string) {
    return this.axios.delete(url);
  }
}
