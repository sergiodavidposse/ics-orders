import { Ifetchers } from './ports/fetchers';

export interface Iinfra {
  fetchers: Ifetchers;
  //persistance: () => fetchers;
}

export class infra implements Iinfra {
  public fetchers: Ifetchers;
  //public persistance: fetchers;
  constructor({fetchers}:{fetchers: Ifetchers}) {
    this.fetchers = fetchers;
  }
}