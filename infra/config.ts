require('dotenv').config()
export const SALES_ORDERS_SERVICE_URL = 
  'https://sos-graphql-api.ecomm.cencosud.com/graphql';
export const SALES_ORDERS_SERVICE_KEY = process.env.SALES_ORDERS_SERVICE_KEY;
