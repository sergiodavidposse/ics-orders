require('dotenv').config();
var exec = require('child_process').exec;

let commit = process.env.COMMIT_MSG.toLowerCase();
//VERSIN PATCH
if (commit.includes('patch')) {
  exec('npm run release -- --release-as patch', () => {});
}
//VERSIN MINOR
if (commit.includes('minor')) {
  exec('npm run release -- --release-as minor', () => {});
}
//VERSIN MAJOR
if (commit.includes('major')) {
  exec('npm run release -- --release-as major', () => {});
}
